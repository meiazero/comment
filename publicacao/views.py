from django.shortcuts import render, redirect
from django.contrib import messages

from .models import Publicacao

def publicar(request):
    if request.method == 'POST':
        titulo = request.POST.get('titulo')
        conteudo = request.POST.get('publicacao')

        if len(titulo) > 0 and len(conteudo) > 0:
            Publicacao.objects.create(
                titulo=titulo,
                conteudo=conteudo,
            )

            messages.success(request, 'Publicação realizada com sucesso!')
            return redirect('home')
        else:
            messages.error(request, 'Preencha todos os campos!')
            return redirect('publicar') 


    return render(request, 'pages/publicar.html')