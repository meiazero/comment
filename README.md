# Comment_highlighter
> This project is a PoC of a comment marker, for an educational platform.

## Installation

### Requirements

- Python 3.8 or higher

### Setup

```sh
pip install -r requirements.txt
```

## Usage

```sh
python manage.py makemigrations
```
> create the models migrations files.

```sh
python manage.py migrate
```
> migrate the migrations to the databse, in PoC case, it's a sqlite3 database.

```sh
python manage.py runserver
```
> run the server, and listen on port 8000.


## Contributing

1. Fork it (project)[https://gitlab.com/meiazero/comment] !
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Merge Request

