from django.db import models
from publicacao.models import Publicacao

class Comentario(models.Model):
    publicacao = models.ForeignKey(Publicacao, on_delete=models.CASCADE)
    comentario = models.TextField()
    selecao = models.TextField()
    destaque_inicio = models.PositiveIntegerField()
    destaque_fim = models.PositiveIntegerField()

    def __str__(self):
        return self.selecao
