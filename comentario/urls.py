from django.urls import path
from . import views


urlpatterns = [
    path('', views.redi),
    path('comentar/<str:id>', views.comentar, name='comentar'),
    path('detalhes-publicacao/<str:publicacao_id>/', views.detalhes_publicacao, name='detalhes_publicacao'),
    path('home/', views.pagina_inicial, name='home'),
    path('comentario/apagar/<int:id>/', views.apagar_comentario, name='apagar_comentario'),
]