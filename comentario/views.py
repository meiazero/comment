from django.shortcuts import render, redirect
from publicacao.models import Publicacao
from .models import Comentario

# creative name to redirect
def redi(request):
    return redirect('home')


def pagina_inicial(request):
    publicacoes = Publicacao.objects.all()

    return render(request, 'pages/home.html', {'publicacoes': publicacoes, 'title': 'Home'})


def comentar(request, id):
    publicacao = Publicacao.objects.get(id=id)


    if request.method == 'POST':
        texto = request.POST.get('texto')
        selecao = request.POST.get('selecao')
        destaque_inicio = int(request.POST.get('destaque_inicio'))
        destaque_fim = int(request.POST.get('destaque_fim'))

        comentario = Comentario(comentario=texto, selecao=selecao, destaque_inicio=destaque_inicio, destaque_fim=destaque_fim, publicacao=publicacao)
        comentario.save()
        return redirect('comentar', id=id) 
    
    return render(request, 'pages/comentar.html', {
        'title': 'Conmentar',
        'publicacao': publicacao
    }) 


def detalhes_publicacao(request, publicacao_id):
    publicacao = Publicacao.objects.get(pk=publicacao_id)
    destaques = Comentario.objects.filter(publicacao=publicacao)

    conteudo_sem_destaque = publicacao.conteudo
    conteudo_com_destaque = publicacao.conteudo

    for comment in destaques:
        if comment.destaque_inicio and comment.destaque_fim:
            inicio = comment.destaque_inicio
            fim = comment.destaque_fim
            parte_destacada = f' <span class="highlight"> {publicacao.conteudo[inicio:fim]} </span> '
            conteudo_com_destaque = conteudo_com_destaque[:inicio] + parte_destacada + conteudo_com_destaque[fim:]

    return render(request, 'pages/detalhes_publicacao.html', {
        'publicacao': publicacao,
        'destaques': destaques,
        'conteudo_sem_destaque': conteudo_sem_destaque,
        'conteudo_com_destaque': conteudo_com_destaque,
    })

def apagar_comentario(request, id):
    comentario = Comentario.objects.get(id=id)
    comentario.delete()
    return redirect('home')
